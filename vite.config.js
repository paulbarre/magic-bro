import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  base: 'https://paulbarre.gitlab.io/magic-bro/',
  plugins: [vue()]
})
